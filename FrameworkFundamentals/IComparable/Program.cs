﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IComparable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("joey".CompareTo("Joey"));
            Console.WriteLine(string.Compare("abc","ABC",StringComparison.CurrentCultureIgnoreCase));
            Console.WriteLine("Beck".CompareTo("Anne")); // 1
            Console.WriteLine("Beck".CompareTo("Beck")); // 0
            Console.WriteLine("Beck".CompareTo("Chris")); // -1

            //chinh cua so console
            Console.WindowWidth = Console.LargestWindowWidth;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("test... 50%");
            Console.CursorLeft -= 3;
            Console.Write("90%"); // test... 90%
            Console.ReadKey();
        }
    }
}
