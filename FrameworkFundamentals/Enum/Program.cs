﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Enum
{
    class Program
    {
        enum EmployeeType
        {
            DE,SE,SSE,PM
        }
        //Enum to integral conversions
        [Flags] public enum BorderSlider
        {
            left = 2,right = 3 ,top = 5,bottom = 4
        }
        static void Main(string[] args)
        {
            Display(EmployeeType.DE);
            int i = (int)BorderSlider.top;

            BorderSlider top = (BorderSlider) i;
            Display(top);

            Console.WriteLine(i);

            
           


            Console.ReadKey();
        }

        static void Display(System.Enum nameEnum)
        {
            Console.WriteLine(nameEnum.GetType().Name +"-"+ nameEnum.ToString());
        }
        //convert enum 
        static decimal GetAnyIntegralValue(System.Enum anyEnum)
        {
            return Convert.ToDecimal(anyEnum);
        }
    }
}
