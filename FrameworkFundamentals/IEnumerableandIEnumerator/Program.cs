﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEnumerableandIEnumerator
{
    /// <summary>
    /// public interface IEnumerator
    //{
    //  bool MoveNext();
    //  object Current { get; }
    //  void Reset();
    //}
    //public interface IEnumerable
    //{
    //    IEnumerator GetEnumerator();
    //}
    //</summary>
    class Program
    {
        //ienumerable minium
        struct Student
        {
            public int id;
            public string name;
        }

        static void Main(string[] args)
        {
            string str = "hello joey";
            IEnumerator enumerator = str.GetEnumerator();
            while (enumerator.MoveNext())
            {
                char c =(char)enumerator.Current;
                Console.Write(c + ".");
            }
          

            //shortcut below
            string s = "Hello"; // The String class implements IEnumerable
            foreach (char c in s)
                Console.Write(c + ".");

            //generic 
            int[] datas = {1,2,3,4,5};
            var rador = ((IEnumerable<int>) datas).GetEnumerator();
            while (rador.MoveNext())
            {
                int value = (int) rador.Current;
                Console.WriteLine(value);
            }

            Student student = new Student
            {
                id = 1,
                name = "joey"
            };

            Student student2 = new Student
            {
                id = 2,
                name = "joey2"
            };
            Student[] students = {student,student2};

            var stuEnumerator = ((IEnumerable<Student>) students).GetEnumerator();

          

            while (stuEnumerator.MoveNext())
            {
                Student current = stuEnumerator.Current;
                Console.WriteLine(current.name);
            }

            //To implement IEnumerable/IEnumerable<T>, you must provide an enumerator


            Console.ReadKey();
        }
    }

    
}
