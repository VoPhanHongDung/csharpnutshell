﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static bool ContainsA(string name) { return name.Contains("a"); }

        static void Main(string[] args)
        {
            //array provide some method 
            // 1 . length 


            // 2. search

            string[] names = { "Rodney", "Jack", "Jill" };
            string match = Array.Find(names, ContainsA);
            Console.WriteLine(match); // Jack

            // 3. sort

            int[] numbers = { 3, 2, 1 };
            string[] words = { "three", "two", "one" };
            Array.Sort(numbers, words);
            // numbers array is now { 1, 2, 3 }
            // words array is now { "one", "two", "three" }

            // 4. reverse
            //public static void Reverse (Array array);
            //public static void Reverse(Array array, int index, int length);

            //5.copy
            //Array provides four methods to perform shallow copying: Clone, CopyTo, Copy and ConstrainedCopy

            //6.Converting and Resizing
            //public delegate TOutput Converter<TInput,TOutput> (TInput input)
            float[] reals = { 1.3f, 1.5f, 1.8f };
            int[] wholes = Array.ConvertAll(reals, r => Convert.ToInt32(r));
            // wholes array is { 1, 2, 2 }

        }
    }
}
