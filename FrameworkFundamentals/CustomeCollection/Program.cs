﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomeCollection
{
    public class Animal
    {
        public string Name;
        public int Popularity;
        public Animal(string name, int popularity)
        {
            Name = name; Popularity = popularity;
        }
    }

    public class AnimalCollection : Collection<Animal>
    {
        // AnimalCollection is already a fully functioning list of animals.
        // No extra code is required.
    }

    public class Zoo // The class that will expose AnimalCollection.
    { // This would typically have additional members.
        public readonly AnimalCollection Animals = new AnimalCollection();
    }
    class Program
    {
        static void Main(string[] args)
        {
            Zoo zoo = new Zoo();
            zoo.Animals.Add(new Animal("Kangaroo", 10));
            zoo.Animals.Add(new Animal("Mr Sea Lion", 20));
            foreach (Animal a in zoo.Animals) Console.WriteLine(a.Name);
            Console.ReadKey();
        }
    }
}
