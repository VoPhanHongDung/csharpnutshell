﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitArray
{
    class Program
    {
        static void Main(string[] args)
        {
            // su dung it bo nho hon vi moi vung nho chi luu 1 gia tri true hay false
            var bits = new System.Collections.BitArray(2);
            bits[1] = true;
            bits[0] = false; 
            bits.Xor(bits); // Bitwise exclusive-OR bits with itself
            Console.WriteLine(bits[1]); // False
            Console.WriteLine(bits[0]); // False
            
            Console.ReadKey();
        }
    }
}
