﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guid
{
    class Program
    {
        static void Main(string[] args)
        {
            // là kiểu số nguyên 128 bit, có thể được xác định cho những thứ là duy nhất, 
            //The Guid struct represents a globally unique identifier: a 16-byte value that, when
            //generated, is almost certainly unique in the world. there is 2128 or 3.4 × 1038 unique Guids
            System.Guid guid = System.Guid.NewGuid();
            Console.WriteLine(guid.ToString());
            System.Guid g1 = new System.Guid("{0d57629c-7d6e-4847-97cb-9e2fc25083fe}");
            Console.WriteLine(g1);

           


            Console.ReadKey();
        }
      
    }
}
