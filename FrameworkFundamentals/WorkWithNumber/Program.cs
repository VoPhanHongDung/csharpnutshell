﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            //math -------------------------------------
           /*   Rounding : Round, Truncate, Floor, Ceiling
                Maximum / minimum : Max, Min
                Absolute value and sign : Abs, Sign
                Square root : Sqrt
                Raising to a power : Pow, Exp
                Logarithm : Log, Log10
                Trigonometric : Sin, Cos, Tan - Sinh, Cosh, Tanh - Asin, Acos, Atan
           */

            double fSqrt = Math.Sqrt(4);
            Console.WriteLine(fSqrt);
            
            //------------------------------------------

            //BigInteger
            BigInteger googol = BigInteger.Pow(10, 100);
            Console.WriteLine(googol);

            string bigNumber = "1".PadRight(10, '0');
            Console.WriteLine(bigNumber);
            BigInteger bigInteger = BigInteger.Parse(bigNumber);
            BigInteger integer = bigInteger + 1;
            Console.WriteLine("hello joey 1 {0}",integer);

            double d = Convert.ToDouble(10000.953.ToString("N3"));
            Console.WriteLine(d);

            //complex
            var c1 = new Complex(2, 3.5);
            Console.WriteLine(c1);
            var c2 = new Complex(3, 0);
            Console.WriteLine(c1.Real); // 2
            Console.WriteLine(c1.Imaginary); // 3.5
            Console.WriteLine(c1.Phase); // 1.05165021254837
            Console.WriteLine(c1.Magnitude); // 4.03112887414927

            Console.WriteLine(c1 + c2); // (5, 3.5)
            Console.WriteLine(c1 * c2); // (6, 10.5) ?

            //random
            Random rNumber = new Random();
            Console.WriteLine(rNumber.Next(100));
            Console.WriteLine(rNumber.Next(1,10)); // min =1 , max =10

           
            

           



            Console.ReadKey();

        }
    }
}
