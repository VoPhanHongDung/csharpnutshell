﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tuples
{
    class Program
    {
        //tuple la dung de luu cai gia tri nhap vao
        static void Main(string[] args)
        {
            var objTuple = new Tuple<int , string>(1,"Joey");
            Console.WriteLine(objTuple.Item1);
            Console.WriteLine(objTuple.Item1 * 2); // 2
            Console.WriteLine(objTuple.Item2.ToUpper()); // JOEY
            //Comparing Tuples
            var t1 = Tuple.Create(123, "Hello");
            var t2 = Tuple.Create(123, "Hello");
            Console.WriteLine(t1 == t2); // False
            Console.WriteLine(t1.Equals(t2)); // True
            Console.WriteLine(string.Compare(t2.Item2,t1.Item2,StringComparison.InvariantCultureIgnoreCase));


            Console.ReadKey();
        }
    }
}
