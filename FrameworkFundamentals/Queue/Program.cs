﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            var queue = new Queue<int>();
            queue.Enqueue(10);
            queue.Enqueue(20);

            int[] data = queue.ToArray();
            foreach (var i in data)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine(queue.Count); // "2"
            Console.WriteLine(queue.Peek()); // "10"
            Console.WriteLine(queue.Dequeue()); // "10"
            Console.WriteLine(queue.Dequeue()); // "20"
            try
            {
                Console.WriteLine(queue.Dequeue()); // throws an exception (queue empty)
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadKey();
        }
    }
}
