﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEquatable
{
    class Program
    {
        static void Main(string[] args)
        {
            Area a1 = new Area(5, 10);
            Area a2 = new Area(10, 5);
            Console.WriteLine(a1.Equals(a2)); // True
            Console.WriteLine(Equals(a1, a2));
            Console.WriteLine(object.Equals(a2,a1));
            Console.ReadKey();
        }
    }

    public struct Area : IEquatable<Area>
    {
        private readonly int Measure1;
        private readonly int Measure2;

        public bool Equals(Area other)
            => Measure1 == other.Measure1 && Measure2 == other.Measure2;

        public override bool Equals(object obj)
        {
            if (!(obj is Area))
            {
                return false;
            }

            return Equals((Area) obj);
        }

        public override int GetHashCode()
            => Measure2 * 31 + Measure1;

        public Area(int measure1, int measure2)
        {
            Measure1 = Math.Min(measure1,measure2);
            Measure2 = Math.Max(measure1, measure2); ;
        }

        
    }
}
