﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkFundamentals
{
    class Program
    {
        //ToUpper, ToLower, and IsWhiteSpace
        static void Main(string[] args)
        {
            char newLine = '\n';
            char c = 'c';
            Console.WriteLine(System.Char.ToUpper(c));

            Console.WriteLine(System.Char.ToUpper('i'));
            Console.WriteLine(char.ToUpper('i') == 'I');
            Console.WriteLine(char.ToUpperInvariant('i'));
            Console.WriteLine(char.IsSymbol('+'));

            Console.ReadKey();
        }
        /* IsSymbol Most other printable symbols
         * IsNumber 0–9 plus digits of other alphabets
         * IsLetter A–Z, a–z, and letters of other alphabets
         * 
         */

    }
}
