﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FormattingandParsing
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = true.ToString(); // s = "True"
            bool b = bool.Parse(s); // b = true
            Console.WriteLine(10.3.ToString("C")); // $10.30
            Console.WriteLine(10.3.ToString("F4")); // 10.3000 (Fix to 4 D.P.)            //Format providers and CultureInfo
            CultureInfo uk = CultureInfo.GetCultureInfo("en-GB");
            Console.WriteLine(3.ToString("C",uk)); // £3.00

            //NumberFormatInfo or DateTimeFormatInfo 
            NumberFormatInfo numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberGroupSeparator = " ";
            Console.WriteLine(12456.6789.ToString("N3",numberFormatInfo));  //.6789 => .679

            //Composite formatting cho phep ket hop bien thay the voi string format 
            string composite = "Credit:{0:C}";
            Console.WriteLine(string.Format(composite,500));// trong phan string 

            //convert 
            double d = 3.9;
            int i = Convert.ToInt32(d); // i == 4
            Console.WriteLine(i);

            string sXML = XmlConvert.ToString(true); // s = "true"
            bool isTrue = XmlConvert.ToBoolean(sXML);
            Console.WriteLine(isTrue);


            Console.ReadKey();
        }
    }
}
