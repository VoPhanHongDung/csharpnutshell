﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareEquailty
{
    class Program
    {

        static void Main(string[] args)
        {
            object obj1 = 5;
            object obj2 = 5;
            Console.WriteLine(obj2 == obj1);
            Console.WriteLine(obj2.Equals(obj1));
            Console.WriteLine(object.Equals(obj2,obj1));
            string s = "a";
            string s2 = "A";
            Console.WriteLine("hello {0}",string.Compare(s,s2,StringComparison.CurrentCultureIgnoreCase));


            //The static object.ReferenceEquals method
            Widget w1 = new Widget();
            Widget w2 = new Widget();
            Widget2 w3 = new Widget2(2);
            Console.WriteLine(object.ReferenceEquals(w1, w3)); // False
            Console.WriteLine(object.Equals(w1, w2));


            Console.ReadKey();

        }
    }

    public class Widget
    {
        
    }

    public class Widget2
    {
        private int a;

        public Widget2(int a)
        {
            this.a = a;
        }
    }
}
