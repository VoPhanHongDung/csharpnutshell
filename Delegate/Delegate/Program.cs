﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    //interface delegates
    public interface ITransform
    {
        int match(int value);
       
    }

    class SquareForITransform : ITransform
    {
        public int match(int value)
        {
            return value * value * value;
        }

    }


    class Program
    {
        delegate int transform(int value);

        delegate void mutiTransform(int value);

        public delegate int areasquare(int length , int width);

        delegate void StringAction(string s);

        delegate object ObjectRetriever();
     
        static void Main(string[] args)
        {
            
            transform tra = matchValue;
            int answer = tra(3);
            Console.WriteLine("number match : {0}",answer);

            mutiTransform mutiTransform = printValueFirst;
            mutiTransform += printValueSecond;

            mutiTransform(1);

            //how to use delegate in class method 
            Square square = new Square()
            {
                length = 10,
                width = 2
            };

            areasquare areaSquare = square.areaSquare;
            int resultSquare =  areaSquare(square.length, square.width);

            Console.WriteLine("result square {0}",resultSquare);

            int[] values = {1, 2, 3};
            //Util.Transfor(values,matchValue);
            //foreach (int value in values)
            //{
            //    Console.WriteLine("value {0}",value);
            //}

            //genericTransform<int> genericTransform = matchValue;
            //int generic = genericTransform(5);
            //Console.WriteLine("generic total {0}",generic);

            //Util.TransforALL(values,new SquareForITransform());
            //foreach (int value in values)
            //{
            //    Console.WriteLine("value {0}", value);
            //}


            //genericTransform<string> str = matchValue;
            //Console.WriteLine(str("hello number"));


            //totalListTransform<int[]>totalListTransform = totalList;
            //totalListTransform(values);

            //Parameter compatibility
            StringAction act = ActOnObject;
            act("hello joey");

            StringAction actStr = new StringAction(ActOnObject);
            actStr("hello joey tribbiani");

            //Return type compatibility
            ObjectRetriever obj = new ObjectRetriever(RetrieveString);
            object resObj = obj();
            Console.WriteLine(resObj);

            Console.WriteLine("runing delegate !");
            Console.ReadKey();

            ////you can have a single method invoked by
            //two different delegates, one passing a MouseEventArgs and the
            //    other passing a KeyEventArgs
        }

        static void ActOnObject(object o) => Console.WriteLine(o);

        static string RetrieveString() => "hello";

        static int matchValue(int number) => number * number;

        static string matchValue(string number) => number;

        static void printValueFirst(int number)
        {
            Console.WriteLine("number print value 1 {0}",number);
        }

        static void printValueSecond(int number)
        {
            Console.WriteLine("number print value 2 {0}", number+2);
        }

        static int[] totalList(int[] values)
        {
            int total = 0;
            foreach (int value in values)
            {
                total += value;
            }

            Console.WriteLine(total);

            return values;
        }
    }

    public class Square
    {
        public int length;
        public int width;

        public int areaSquare(int length , int width) => length * width;
  
    }
    //Generic Delegate Types
    public delegate T genericTransform<T>(T value);

    public delegate T totalListTransform<T>(T value);

    public class Util
    {
        //generic 
        public static void Transfor<T>(T[] values , genericTransform<T> genericTransform )
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = genericTransform(values[i]);
            }
        }

        //interface 
        public static void TransforALL(int[] values, ITransform iTransform)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = iTransform.match(values[i]);
            }
        }
        
       


    }
}
