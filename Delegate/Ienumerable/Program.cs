﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ienumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            // enumerable 
            // yield return trả về kết quả ngay tức thì ( từ từ duyệt hết mãng ) 

            //foreach (int i in PrintNuEnumerable() )
            //{
            //    Console.WriteLine(i);
            //}

            //foreach (int i in PrintNuEnumerable2(true))
            //{
            //    Console.WriteLine(i);
            //}

            //string str = null;
            //var sequence = finalEnumerable();

            //using (var status = sequence.GetEnumerator())
            //{
            //    if (status.MoveNext())
            //    {
            //        str = status.Current;
            //    }
            //}


            //Console.WriteLine(str);
            foreach (int fib in Fibs(6))
                Console.WriteLine(fib);

            Console.WriteLine("------------------------");

            foreach (int fib in EvenNumbersOnly(Fibs(6)))
                Console.WriteLine(fib);


            Console.ReadKey();
        }

        public static IEnumerable<int> PrintNuEnumerable()
        {
            yield return 10;
            yield return 5;
            yield return 3;
        }

        //yield break return early
        public static IEnumerable<int> PrintNuEnumerable2(bool flag)
        {
            yield return 10;
            yield return 5;
            if (flag) yield break;
            yield return 3;
        }
        // try/catch/finally blocks

        static IEnumerable<string> finalEnumerable()
        {
            //khong dung catch cho yield return 
            try
            {
                yield return "one";
            }
            finally
            {

            }
        }

        static IEnumerable<int> Fibs(int fibCount)
        {
            for (int i = 0, prevFib = 1, curFib = 1; i < fibCount; i++)
            {
                yield return prevFib;
                int newFib = prevFib + curFib;
                prevFib = curFib;
                curFib = newFib;
            }
        }

        static IEnumerable<int> EvenNumbersOnly(IEnumerable<int> sequence)
        {


            foreach (int x in sequence)
            {
                Console.WriteLine("value x {0}", x);
                if ((x % 2) == 0)

                    yield return x;
            }
        }


    }


    //sScenarios for Nullable Types
    public class Row
    {
        public decimal? parent;
        public decimal? color;
    }


}
