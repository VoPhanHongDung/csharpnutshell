﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lamda
{
    public delegate int Transformer (int x);
    class Program
    {
        static void Main(string[] args)
        {
            //Transformer transformer = x => x * x;
            ////Console.WriteLine("total lamda : {0}",transformer(3));
            //string fullname = mapString("Joey", "Tribbiani");
            //Console.WriteLine(fullname);


            ////A lambda expression can reference the local variables and parameters of the method
            ////in which it’s defined
            //int factory = 5;
            //Func<int, int> easyFunny = n => n * factory;
            //int ressult = easyFunny(3);
            //Console.WriteLine(ressult);

            ////Lambda expressions can themselves update captured variables:
            ////capture variable
            //int seed = 0;
            //Func<int> natural = () => seed++;
            //Console.WriteLine(natural()); // 0
            //Console.WriteLine(natural()); // 1
            //Console.WriteLine(seed); // 2

            //Action[] actions = new Action[3];
            //for (int i = 0; i < actions.Length; i++)
            //{
            //    actions[i] = () => Console.WriteLine(i);
            //    //Console.WriteLine("{0}",i);
            //}
            //foreach (Action a in actions) a(); //333

            ////vì thằng closure nó lưu capture variable nên giải pháp là
            //Action[] actions2 = new Action[3];
            //for (int i = 0; i < 3; i++)
            //{
            //    int loopScopedi = i;
            //    actions2[i] = () => Console.Write(loopScopedi);
            //}
            //foreach (Action b in actions2) b(); // 012

            //Action[] actions = new Action[3];
            //int i = 0;
            //foreach (char c in "abc")
            //    actions[i++] = () => Console.WriteLine(c);
            //foreach (Action a in actions) a();






            //Console.ReadKey();
    
        }

        public static Func<string, string, string> mapString = (s1, s2) => s1+ s2;
       
    }

   
}
