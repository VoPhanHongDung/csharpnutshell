﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            var newStudent = new Student("Joey", 23);

            int Age = 22;
            //Age
            string Name = "Joey Tribbiani";
            var newStudent2 = new { Name, Age};
           
            Console.WriteLine("{0},{1}", newStudent2.Name, newStudent2.Age);

            //Two anonymous type
            //type 1 
            var a1 = new { X = 2, Y = 4 };
            var student = new {Name = "joey", Id = 1};
            var a2 = new { X = 2, Y = 4 };
            Console.WriteLine(a1.GetType() == a2.GetType());

            //type 2 create arrays of anonymous types
            var dudes = new[]
                {
                    new { Name = "Bob", Age = 30 },
                    new { Name = "Tom", Age = 40 }
                };

            foreach (var dude in dudes)
            {
                Console.WriteLine(dude.Name);
            }
            //----------------------

            Console.ReadKey();
        }
    }
    //internal
    internal class Student
    {
        private string name; // Actual field name is irrelevant
        private int age;

        public Student(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public string Name
        { //anonymous types
            get { return name; }
        }
        //Age  
        public int Age
        {
            get { return age; }
        }
    }
}
