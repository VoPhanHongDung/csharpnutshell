﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Exception
{
    class Program
    {
        static int Calc(int x) =>  10 / x;
        static void Main(string[] args)
        {
            try
            {
                int y = Calc(0);
                Console.WriteLine(y);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("x cannot be zero ");
            }
            catch (WebException ex) when(ex.Status == WebExceptionStatus.Timeout)
            {
               Console.WriteLine(ex.Status);
            }
            Console.ReadKey();
        }
    }
}
