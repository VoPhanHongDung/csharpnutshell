﻿using System;
using System.Net;
using System.Xml;

namespace ThrowExceptions
{
    class Program
    {
        public static void Display(string str)
        {
            if(str == null)
                throw new ArgumentNullException(nameof(str));
            Console.WriteLine(str);
        }
        static void Main(string[] args)
        {
            //try
            //{
            //    Display(null);
            //}
            //catch (ArgumentNullException e)
            //{
            //    Console.WriteLine("Caught the exception");
               
            //}

            string s = null;
            //không gian tên cho việc tham chiếu, từ khóa using còn được sử dụng để tự động gọi phương thức Dispose() 
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    s = webClient.DownloadString("http://www.albahari.com/nutshell/");
                }
                //catch (WebException e)
                //{
                //    if (e.Status == WebExceptionStatus.Timeout)
                //    {
                //        Console.WriteLine("time out");
                //    }
                //    else
                //    {
                //        throw;
                //    }

                //    throw;
                //}
                // cach viet ngan gon more tersely
                catch (WebException e) when (e.Status == WebExceptionStatus.Timeout)
                {
                    Console.WriteLine("time out");
                }
                

            }

            //detail exceptions 
            try
            {
                // Parse a DateTime from XML element data
            }
            catch (FormatException ex)
            {
                throw new XmlException("Invalid DateTime", ex);
            }
            /*
             * StackTrace :
                A string representing all the methods that are called from the origin of the
                exception to the catch block.
                Message : 
                A string with a description of the error.
                InnerException :
                The inner exception (if any) that caused the outer exception. This, itself,
                may have another InnerException.
             */

            /* type exceptions
             * System.ArgumentException throw when  a bogus argument ( đối số không có thật )             * System.ArgumentNullException Subclass of ArgumentException that’s thrown when a function argument is (unexpectedly) null
             * System.ArgumentOutOfRangeException Subclass of ArgumentException that’s thrown when a (usually numeric) argument is too big or too small. For example, this is thrown when passing a negative number into a function that accepts only positive values.
             * System.InvalidOperationException Thrown when the state of an object is unsuitable for a method to success‐fully execute, regardless of any particular argument values. 
             *  System.NotSupportedException Thrown to indicate that a particular functionality is not supported
             * System.NotImplementedException Thrown to indicate that a function has not yet been implemented
             * System.ObjectDisposedException Thrown when the object upon which the function is called has been disposed
             */

            Console.ReadKey();
        }
    }
}
